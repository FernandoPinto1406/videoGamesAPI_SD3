﻿using System.Net;
using JalaU.VideoGamesAPI.System.Core.Domain;

namespace JalaU.VideoGamesAPI.System.Core.Application;

public class EntityValidatorUtil<T>
{
    public static void ValidateNonNullEntity(T? objectToValidate, string possibleErrorMessage)
    {
        if (objectToValidate == null)
        {
            throw new EntityNotFoundException(possibleErrorMessage);
        }
    }

    public static List<MessageLogDTO> ValidateEntityFields(T objectToValidate)
    {
        List<MessageLogDTO> messageLogs = [];

        var properties = objectToValidate!.GetType().GetProperties();

        foreach (var property in properties)
        {
            var value = property.GetValue(objectToValidate);

            if (value == null || string.IsNullOrWhiteSpace(value.ToString()))
            {
                messageLogs.Add(
                    new MessageLogDTO(
                        (int)HttpStatusCode.UnprocessableEntity,
                        $"The field {property.Name} can't be null or empty"
                    )
                );
            }
        }

        return messageLogs;
    }

    public List<MessageLogDTO> ValidateDuplicateField(T newField, T oldField)
    {
        List<MessageLogDTO> messageLogs = [];
        if (EqualityComparer<T>.Default.Equals(newField, oldField))
        {
            messageLogs.Add(
                new MessageLogDTO(
                    (int)HttpStatusCode.Conflict,
                    $"The entity with the field '{newField}' already exists."
                )
            );
        }
        return messageLogs;
    }
}
