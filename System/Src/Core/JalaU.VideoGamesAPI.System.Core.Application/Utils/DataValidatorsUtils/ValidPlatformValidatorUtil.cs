﻿using System.Net;
using JalaU.VideoGamesAPI.System.Core.Domain;

namespace JalaU.VideoGamesAPI.System.Core.Application;

public class ValidPlatformValidatorUtil
{
    public static List<MessageLogDTO> ValidateSupportedPlatform(string platform)
    {
        List<MessageLogDTO> messages = [];
        if (!Enum.TryParse(platform, out ValidPlatforms validPlatform))
        {
            messages.Add(
                new MessageLogDTO(
                    (int)HttpStatusCode.UnprocessableEntity,
                    $"The field platform doesn't match the valid platforms."
                )
            );
        }
        return messages;
    }
}
