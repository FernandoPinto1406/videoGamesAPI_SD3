﻿using System.Net;
using JalaU.VideoGamesAPI.System.Core.Domain;

namespace JalaU.VideoGamesAPI.System.Core.Application;

public class PasswordValidatorUtil
{
    public static List<MessageLogDTO> ValidateUserPassword(string password)
    {
        List<MessageLogDTO> messages = [];

        if (password.Length <= 8)
        {
            messages.Add(
                new MessageLogDTO(
                    (int)HttpStatusCode.UnprocessableEntity,
                    "The given password must have more than 8 characters."
                )
            );
        }

        if (!password.Any(char.IsDigit))
        {
            messages.Add(
                new MessageLogDTO(
                    (int)HttpStatusCode.UnprocessableEntity,
                    "The given password must have at least one number."
                )
            );
        }

        return messages;
    }
}
