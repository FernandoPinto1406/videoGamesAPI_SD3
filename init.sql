CREATE DATABASE VideoGamesDB;
USE VideoGamesDB;

CREATE TABLE VideoGames
(
    id              VARCHAR(36) NOT NULL PRIMARY KEY,
    name            VARCHAR(200) NOT NULL,
    platform        VARCHAR(200) NOT NULL,
    price           DOUBLE NOT NULL,
    UNIQUE INDEX `id_UNIQUE` (id ASC)
);

CREATE TABLE Users
(
    id              VARCHAR(36) NOT NULL PRIMARY KEY,
    username        VARCHAR(100) NOT NULL,
    password        VARCHAR(100) NOT NULL,
    role            VARCHAR(100) NOT NULL,
    UNIQUE INDEX `id_UNIQUE` (id ASC)
)

INSERT INTO VideoGames (id, name, platform, price)
VALUES ('d290f1ee-6c54-4b01-90e6-d701748f0851', 'Minecraft', 'XBOX', 55.25);

INSERT INTO Users (id, username, password, role) 
VALUES (UUID(), 'admin_example', 'password123', 'ADMIN');

INSERT INTO Users (id, username, password, role) 
VALUES (UUID(), 'normal_user', 'password333333', 'USER');

