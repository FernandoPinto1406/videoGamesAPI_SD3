using System.Net;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using JalaU.VideoGamesAPI.System.Core.Domain;
using JalaU.VideoGamesAPI.System.Data.DbConnection;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace JalaU.VideoGamesAPI.Integration.Test
{
    [TestFixture]
    public class VideoGameIntegrationTest
    {
        private const string TEST_USERNAME = "TestUser";
        private const string TEST_USER_PASSWORD = "password123";
        private const string TEST_USER_ROLE = "ADMIN";
        private const string FIRST_VIDEOGAME_NAME = "Stardew Valley";
        private const string SECOND_VIDEOGAME_NAME = "Plants Vs. Zombies";

        private WebApplicationFactory<Program> _factory;

        [SetUp]
        public void Setup()
        {
            _factory = new WebApplicationFactory<Program>();
        }

        [TearDown]
        public void TearDown()
        {
            _factory.Dispose();
        }

        [Test]
        public async Task OnPostTwoVideoGames_ShouldSaveAllVideoGames()
        {
            await CleanupTestDataAsync();
            var client = _factory.CreateClient();

            var testJwtToken = await RegisterTestUserAsync(client);
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(
                "Bearer",
                testJwtToken
            );
            List<VideoGame> videoGamesCreated = [];
            List<string> createdVideoGamesNames = [];
            var expectedVideoGameNames = new List<string>
            {
                FIRST_VIDEOGAME_NAME,
                SECOND_VIDEOGAME_NAME
            };

            var createdVideoGames = await CreateTestVideoGamesAsync(client);

            foreach (var gameResponse in createdVideoGames)
            {
                var createdVideoGame = await gameResponse.Content.ReadFromJsonAsync<VideoGame>();

                if (createdVideoGame != null)
                {
                    videoGamesCreated.Add(createdVideoGame);
                    createdVideoGamesNames.Add(createdVideoGame.Name);
                }
            }

            List<VideoGame> videoGamesInDatabase = GetVideoGamesCreatedTestAsync(client).Result;

            Assert.That(createdVideoGames, Is.Not.Null);
            Assert.That(createdVideoGames, Has.Count.EqualTo(2));
            CollectionAssert.AreEquivalent(videoGamesCreated, videoGamesInDatabase);
            CollectionAssert.AreEquivalent(expectedVideoGameNames, createdVideoGamesNames);

            await CleanupTestDataAsync();
        }

        private async Task<string> RegisterTestUserAsync(HttpClient client)
        {
            UserRegistrationRequestDTO userRegistrationRequest =
                new()
                {
                    UserName = TEST_USERNAME,
                    Password = TEST_USER_PASSWORD,
                    Role = TEST_USER_ROLE
                };
            var response = await client.PostAsJsonAsync(
                "/videogames-api/v1/users",
                userRegistrationRequest
            );
            response.EnsureSuccessStatusCode();

            var registrationResult =
                await response.Content.ReadFromJsonAsync<AuthenticationResponseDTO>();
            return registrationResult!.Token;
        }

        private async Task<List<VideoGame>> GetVideoGamesCreatedTestAsync(HttpClient client)
        {
            var response = await client.GetAsync("/videogames-api/v1/videoGames");
            response.EnsureSuccessStatusCode();
            var result = await response.Content.ReadFromJsonAsync<GetAllVideoGamesResponseDTO>();
            List<VideoGame> createdVideoGames = [];

            if (result?.VideoGames != null)
            {
                foreach (var item in result.VideoGames)
                {
                    if (item.Name == FIRST_VIDEOGAME_NAME || item.Name == SECOND_VIDEOGAME_NAME)
                    {
                        createdVideoGames.Add(item);
                    }
                }
            }

            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            return createdVideoGames;
        }

        private async Task<List<HttpResponseMessage>> CreateTestVideoGamesAsync(HttpClient client)
        {
            var videoGame1Response = await client.PostAsJsonAsync(
                "/videogames-api/v1/videoGames",
                new VideoGameRequestDTO
                {
                    Name = FIRST_VIDEOGAME_NAME,
                    Platform = "XBOX",
                    Price = 55.65
                }
            );
            var videoGame2Response = await client.PostAsJsonAsync(
                "/videogames-api/v1/videoGames",
                new VideoGameRequestDTO
                {
                    Name = SECOND_VIDEOGAME_NAME,
                    Platform = "PLAY_STATION",
                    Price = 34.77
                }
            );

            List<HttpResponseMessage> videoGamesResponses =
            [
                videoGame1Response,
                videoGame2Response
            ];

            foreach (var videoGameResponse in videoGamesResponses)
            {
                Assert.That(videoGameResponse.StatusCode, Is.EqualTo(HttpStatusCode.Created));
            }

            return videoGamesResponses;
        }

        private async Task CleanupTestDataAsync()
        {
            var services = _factory.Services;
            using var scope = services.CreateScope();
            var dbContext = scope.ServiceProvider.GetRequiredService<AppDbContext>();

            var videoGamesToDelete = await dbContext
                .VideoGames.Where(
                    vg => vg.Name == FIRST_VIDEOGAME_NAME || vg.Name == SECOND_VIDEOGAME_NAME
                )
                .ToListAsync();
            dbContext.VideoGames.RemoveRange(videoGamesToDelete);

            var userToDelete = await dbContext.Users.FirstOrDefaultAsync(
                u => u.UserName == TEST_USERNAME
            );
            if (userToDelete != null)
            {
                dbContext.Users.Remove(userToDelete);
            }

            await dbContext.SaveChangesAsync();
        }
    }
}
