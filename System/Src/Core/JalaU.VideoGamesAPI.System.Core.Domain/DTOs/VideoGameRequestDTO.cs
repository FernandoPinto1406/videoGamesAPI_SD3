﻿namespace JalaU.VideoGamesAPI.System.Core.Domain;

public class VideoGameRequestDTO : BaseRequestDTO
{
    public string? Name { get; set; }
    public string? Platform { get; set; }
    public double Price { get; set; }
}
