﻿using System.Security.Cryptography;
using System.Text;

namespace JalaU.VideoGamesAPI.System.Core.Application;

public class PasswordEncrypterUtil
{
    public static string EncryptPassword(string password)
    {
        ASCIIEncoding encoding = new();
        byte[] stream;
        StringBuilder stringBuilder = new();
        stream = SHA256.HashData(encoding.GetBytes(password));
        for (int index = 0; index < stream.Length; index++)
            stringBuilder.AppendFormat("{0:x2}", stream[index]);
        return stringBuilder.ToString();
    }
}
