﻿using JalaU.VideoGamesAPI.System.Core.Domain;

namespace JalaU.VideoGamesAPI.System.Data.DbConnection;

public class VideoGameRepository(AppDbContext appDbContext) : IRepository<VideoGame>
{
    private readonly AppDbContext _appDbContext = appDbContext;

    public IEnumerable<VideoGame> GetAllEntities()
    {
        return [.. _appDbContext.VideoGames];
    }

    public VideoGame? GetEntityByCriteria(Func<VideoGame, bool> criteria)
    {
        var videoGame = this._appDbContext.VideoGames.FirstOrDefault(criteria);
        return videoGame;
    }

    public VideoGame SaveEntity(VideoGame videoGame)
    {
        _appDbContext.VideoGames.Add(videoGame);
        _appDbContext.SaveChanges();
        return videoGame;
    }

    public VideoGame UpdateEntity(VideoGame videoGame)
    {
        _appDbContext.VideoGames.Update(videoGame);
        _appDbContext.SaveChanges();
        return videoGame;
    }

    public VideoGame DeleteEntity(VideoGame entity)
    {
        _appDbContext.VideoGames.Remove(entity);
        _appDbContext.SaveChanges();
        return entity;
    }
}
