﻿namespace JalaU.VideoGamesAPI.System.Core.Domain;

public class AuthenticationResponseDTO(string token)
{
    public string Token { get; } = token;
}
