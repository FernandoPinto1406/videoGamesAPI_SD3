﻿using JalaU.VideoGamesAPI.System.Core.Domain;
using Microsoft.Extensions.DependencyInjection;

namespace JalaU.VideoGamesAPI.System.Core.Application;

public static class DependencyInjection
{
    public static IServiceCollection AddApplication(this IServiceCollection services)
    {
        services.AddScoped<IVideoGameService, VideoGameService>();
        services.AddScoped<IAuthService, AuthService>();

        return services;
    }
}
