﻿using System.Net;

namespace JalaU.VideoGamesAPI.System.Core.Domain;

public class InvalidCredentialsException(string message)
    : ApplicationException(message),
        IAbstractApiException
{
    public HttpStatusCode StatusCode() => HttpStatusCode.BadRequest;
}
