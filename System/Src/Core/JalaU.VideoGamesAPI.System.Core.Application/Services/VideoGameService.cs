﻿using AutoMapper;
using JalaU.VideoGamesAPI.System.Core.Domain;

namespace JalaU.VideoGamesAPI.System.Core.Application;

public class VideoGameService : IVideoGameService
{
    private readonly IRepository<VideoGame> _repository;
    private readonly Mapper _videoGameMapper;
    private readonly VideoGameValidatorUtil videoGameValidatorUtil;

    public VideoGameService(IRepository<VideoGame> repository)
    {
        _repository = repository;
        this.videoGameValidatorUtil = new();
        var _configVideoGame = new MapperConfiguration(configuration =>
        {
            configuration.CreateMap<VideoGameRequestDTO, VideoGame>().ReverseMap();
        });

        _videoGameMapper = new Mapper(_configVideoGame);
    }

    public List<VideoGame> GetAllVideoGames()
    {
        IEnumerable<VideoGame>? videoGames = _repository.GetAllEntities();
        if (!videoGames.Any())
        {
            throw new EntityNotFoundException("Any Video Game was found.");
        }
        return _videoGameMapper.Map<List<VideoGame>>(videoGames.ToList());
    }

    public VideoGame GetVideoGameByCriteria(string field, string valueToSearch)
    {
        var videoGame =
            field.ToLower() switch
            {
                "id" => this.GetById(valueToSearch),
                "name" => this.GetByName(valueToSearch),
                _ => throw new ArgumentException("Invalid field."),
            }
            ?? throw new EntityNotFoundException(
                $"Topic with the field {field} and the value {valueToSearch} was not found."
            );
        return videoGame;
    }

    private VideoGame? GetById(string guid)
    {
        Guid validGuid = GuidConverterUtil.ValidateGuid(guid);
        return this._repository.GetEntityByCriteria(videoGame => videoGame.Id == validGuid);
    }

    private VideoGame? GetByName(string valueToSearch)
    {
        return this._repository.GetEntityByCriteria(videoGame => videoGame.Name == valueToSearch);
    }

    public VideoGame SaveVideoGame(VideoGameRequestDTO videoGameDto)
    {
        VideoGame? duplicatedVideoGame = null;
        if (videoGameDto.Name != null)
        {
            duplicatedVideoGame = GetByName(videoGameDto.Name);
        }

        List<MessageLogDTO> messageLogDTOs = videoGameValidatorUtil.ValidateEntityToSave(
            duplicatedVideoGame,
            videoGameDto
        );
        if (messageLogDTOs.Count > 0)
        {
            throw new WrongDataException("errors", messageLogDTOs);
        }

        var videoGame = _videoGameMapper.Map<VideoGame>(videoGameDto);

        return _repository.SaveEntity(videoGame);
    }

    public VideoGame UpdateVideoGameById(VideoGameRequestDTO videoGameDto, string id)
    {
        var existingVideoGame = this.GetVideoGameByCriteria("id", id);

        VideoGame? duplicatedVideoGame = null;
        if (videoGameDto.Name != null)
        {
            duplicatedVideoGame = GetByName(videoGameDto.Name);
        }

        List<MessageLogDTO> messageLogs = videoGameValidatorUtil.ValidateEntityToUpdate(
            duplicatedVideoGame,
            videoGameDto
        );

        if (messageLogs.Count > 0)
        {
            throw new WrongDataException("errors", messageLogs);
        }

        existingVideoGame = UpdatableEntityUtil<VideoGame>.UpdateEntities(
            existingVideoGame,
            videoGameDto
        );

        return _repository.UpdateEntity(existingVideoGame);
    }

    public VideoGame DeleteVideoGameById(string id)
    {
        var videoGame = this.GetVideoGameByCriteria("id", id);

        return _repository.DeleteEntity(videoGame)!;
    }
}
