﻿namespace JalaU.VideoGamesAPI.System.Core.Domain;

public enum UserRoles
{
    USER,
    ADMIN
}
