﻿using JalaU.VideoGamesAPI.System.Core.Domain;

namespace JalaU.VideoGamesAPI.System.Core.Application;

public class UpdatableEntityUtil<T>
{
    public static T UpdateEntities(T entityToUpdate, BaseRequestDTO newVersionOfEntity)
    {
        if (entityToUpdate != null && newVersionOfEntity != null)
        {
            var properties = newVersionOfEntity.GetType().GetProperties();

            foreach (var property in properties)
            {
                var newValue = property.GetValue(newVersionOfEntity);
                if (newValue != null)
                {
                    var entityToUpdateProperty = entityToUpdate
                        .GetType()
                        .GetProperty(property.Name);
                    if (
                        entityToUpdateProperty != null
                        && entityToUpdateProperty.PropertyType == property.PropertyType
                        && entityToUpdateProperty.CanWrite
                    )
                    {
                        entityToUpdateProperty.SetValue(entityToUpdate, newValue);
                    }
                }
            }

            return entityToUpdate;
        }

        throw new ApplicationException("One of the Updatable objects is null.");
    }
}
