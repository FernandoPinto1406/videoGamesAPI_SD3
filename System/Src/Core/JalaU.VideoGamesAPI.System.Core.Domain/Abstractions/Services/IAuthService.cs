﻿namespace JalaU.VideoGamesAPI.System.Core.Domain;

public interface IAuthService
{
    string RegisterUser(UserRegistrationRequestDTO userRegistrationDTO);
    string AuthenticateUser(UserAuthenticationRequestDTO userAuthDTO);
}
