using JalaU.VideoGamesAPI.System.Core.Application;
using JalaU.VideoGamesAPI.System.Core.Domain;
using Moq;

namespace JalaU.VideoGamesAPI.Test.Core.Application;

[TestFixture]
public class VideoGameServiceTest
{
    private Mock<IRepository<VideoGame>> _mockRepository;
    private VideoGameService _videoGameService;

    [SetUp]
    public void Setup()
    {
        _mockRepository = new Mock<IRepository<VideoGame>>();
        _videoGameService = new VideoGameService(_mockRepository.Object);
    }

    [Test]
    public void TestSaveVideoGameWithValidData()
    {
        var videoGameDto = new VideoGameRequestDTO
        {
            Name = "Test Game",
            Platform = "XBOX",
            Price = 50.0
        };
        var savedVideoGame = new VideoGame
        {
            Id = Guid.NewGuid(),
            Name = videoGameDto.Name,
            Platform = videoGameDto.Platform,
            Price = videoGameDto.Price
        };

        _mockRepository
            .Setup(repo => repo.GetEntityByCriteria(It.IsAny<Func<VideoGame, bool>>()))
            .Returns((VideoGame)null!);
        _mockRepository
            .Setup(repo => repo.SaveEntity(It.IsAny<VideoGame>()))
            .Returns(savedVideoGame);

        var result = _videoGameService.SaveVideoGame(videoGameDto);

        Assert.Multiple(() =>
        {
            Assert.That(result.Name, Is.EqualTo(videoGameDto.Name));
            Assert.That(result.Platform, Is.EqualTo(videoGameDto.Platform));
            Assert.That(result.Price, Is.EqualTo(videoGameDto.Price));
        });
    }

    [Test]
    public void TestSaveVideoGameWithExistingName()
    {
        var videoGame = new VideoGame
        {
            Id = Guid.NewGuid(),
            Name = "Existing Game",
            Platform = "XBOX",
            Price = 50.0
        };

        var videoGameDto = new VideoGameRequestDTO
        {
            Name = "Existing Game",
            Platform = "PLAY_STATION",
            Price = 70.0
        };

        _mockRepository
            .Setup(repo => repo.GetEntityByCriteria(It.IsAny<Func<VideoGame, bool>>()))
            .Returns(videoGame);

        Assert.Throws<WrongDataException>(() => _videoGameService.SaveVideoGame(videoGameDto));
    }

    [Test]
    public void TestSaveVideoGameWithInvalidPlatform()
    {
        var videoGameDto = new VideoGameRequestDTO
        {
            Name = "Existing Game",
            Platform = "Nintendo",
            Price = 50.0
        };

        Assert.Throws<WrongDataException>(() => _videoGameService.SaveVideoGame(videoGameDto));
    }

    [Test]
    public void TestUpdateVideoGameByIdHappyPath()
    {
        Guid videoGameId = Guid.NewGuid();
        var videoGameDto = new VideoGameRequestDTO
        {
            Name = "Updated Game",
            Platform = "PLAY_STATION",
            Price = 60.0
        };
        var existingVideoGame = new VideoGame
        {
            Id = videoGameId,
            Name = "Existing Game",
            Platform = "XBOX",
            Price = 50.0
        };
        var updatedVideoGame = new VideoGame
        {
            Id = videoGameId,
            Name = videoGameDto.Name,
            Platform = videoGameDto.Platform,
            Price = videoGameDto.Price
        };

        _mockRepository
            .Setup(
                repo =>
                    repo.GetEntityByCriteria(
                        It.Is<Func<VideoGame, bool>>(criteria => criteria(existingVideoGame))
                    )
            )
            .Returns(existingVideoGame);

        _mockRepository
            .Setup(
                repo =>
                    repo.GetEntityByCriteria(
                        It.Is<Func<VideoGame, bool>>(criteria => !criteria(existingVideoGame))
                    )
            )
            .Returns((VideoGame)null!);
        _mockRepository
            .Setup(repo => repo.UpdateEntity(It.IsAny<VideoGame>()))
            .Returns(updatedVideoGame);

        var result = _videoGameService.UpdateVideoGameById(
            videoGameDto,
            existingVideoGame.Id.ToString()
        );

        Assert.Multiple(() =>
        {
            Assert.That(result.Name, Is.EqualTo(videoGameDto.Name));
            Assert.That(result.Platform, Is.EqualTo(videoGameDto.Platform));
            Assert.That(result.Price, Is.EqualTo(videoGameDto.Price));
        });
    }

    [Test]
    public void TestUpdateVideoGameByIdWithExistingName()
    {
        Guid guidVideoGame = Guid.NewGuid();
        var videoGame = new VideoGame
        {
            Id = guidVideoGame,
            Name = "Existing Game",
            Platform = "PLAY_STATION",
            Price = 50.0
        };

        var videoGameDto = new VideoGameRequestDTO
        {
            Name = "Existing Game",
            Platform = "PLAY_STATION",
            Price = 50.0
        };

        _mockRepository
            .Setup(repo => repo.GetEntityByCriteria(It.IsAny<Func<VideoGame, bool>>()))
            .Returns(videoGame);

        _mockRepository
            .Setup(repo => repo.GetEntityByCriteria(It.IsAny<Func<VideoGame, bool>>()))
            .Returns(videoGame);

        Assert.Throws<WrongDataException>(
            () => _videoGameService.UpdateVideoGameById(videoGameDto, guidVideoGame.ToString())
        );
    }
}
