﻿using System.Net;
using JalaU.VideoGamesAPI.System.Core.Domain;

namespace JalaU.VideoGamesAPI.System.Core.Application;

public class ValidUserRoleValidatorUtil
{
    public static List<MessageLogDTO> ValidateSupportedUserRole(string possibleUserRole)
    {
        List<MessageLogDTO> messages = [];
        if (!Enum.TryParse(possibleUserRole, out UserRoles userRoles))
        {
            messages.Add(
                new MessageLogDTO(
                    (int)HttpStatusCode.UnprocessableEntity,
                    $"The field Role doesn't match the valid roles."
                )
            );
        }
        return messages;
    }
}
