﻿using System.Net;
using JalaU.VideoGamesAPI.System.Core.Domain;

namespace JalaU.VideoGamesAPI.System.Core.Application;

public class PositiveNumberValidatorUtil
{
    public static List<MessageLogDTO> ValidatePositiveNumber<T>(string field, T number)
        where T : IComparable<T>
    {
        List<MessageLogDTO> messages = [];

        if (Comparer<T>.Default.Compare(number, default) < 0)
        {
            messages.Add(
                new MessageLogDTO(
                    (int)HttpStatusCode.UnprocessableEntity,
                    $"The given {field} must be positive."
                )
            );
        }
        return messages;
    }
}
