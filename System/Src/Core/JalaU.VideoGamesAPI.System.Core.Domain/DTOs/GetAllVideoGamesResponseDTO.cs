﻿namespace JalaU.VideoGamesAPI.System.Core.Domain;

public class GetAllVideoGamesResponseDTO(int count, List<VideoGame> videoGames)
{
    public int Count { get; set; } = count;
    public List<VideoGame> VideoGames { get; set; } = videoGames;
}
