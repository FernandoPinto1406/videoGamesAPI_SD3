﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace JalaU.VideoGamesAPI.System.Core.Domain;

[Index(nameof(Name), IsUnique = true)]
public class VideoGame
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    [Column("id")]
    public required Guid Id { get; set; }

    [Column("name")]
    [StringLength(200)]
    public required string Name { get; set; }

    [Column("platform")]
    [StringLength(200)]
    public required string Platform { get; set; }

    [Column("price")]
    [Range(0, double.MaxValue, ErrorMessage = "The price must be a positive number.")]
    public required double Price { get; set; }

    public override bool Equals(object? obj)
    {
        if (obj == null || GetType() != obj.GetType())
        {
            return false;
        }

        var other = (VideoGame)obj;
        return Id == other.Id;
    }

    public override int GetHashCode()
    {
        return Id.GetHashCode();
    }
}
