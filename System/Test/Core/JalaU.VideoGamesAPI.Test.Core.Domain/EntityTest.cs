using JalaU.VideoGamesAPI.System.Core.Domain;

namespace JalaU.VideoGamesAPI.Test.Core.Domain;

[TestFixture]
public class EntityTest
{
    [Test]
    public void TestEqualsTwoEqualVideoGames()
    {
        var id = Guid.NewGuid();
        var videoGame1 = new VideoGame
        {
            Id = id,
            Name = "Test Game",
            Platform = "Test Platform",
            Price = 50.0
        };
        var videoGame2 = new VideoGame
        {
            Id = id,
            Name = "Test Game",
            Platform = "Test Platform",
            Price = 50.0
        };

        var result = videoGame1.Equals(videoGame2);
        Assert.That(result, Is.True);
    }

    [Test]
    public void TestEqualsTwoDifferentVideoGames()
    {
        var videoGame1 = new VideoGame
        {
            Id = Guid.NewGuid(),
            Name = "Test Game 1",
            Platform = "Test Platform 1",
            Price = 50.0
        };
        var videoGame2 = new VideoGame
        {
            Id = Guid.NewGuid(),
            Name = "Test Game 2",
            Platform = "Test Platform 2",
            Price = 60.0
        };
        var result = videoGame1.Equals(videoGame2);

        Assert.That(result, Is.False);
    }

    [Test]
    public void TestGetHashCodeTwoEqualVideoGames()
    {
        var id = Guid.NewGuid();
        var videoGame1 = new VideoGame
        {
            Id = id,
            Name = "Test Game",
            Platform = "Test Platform",
            Price = 50.0
        };
        var videoGame2 = new VideoGame
        {
            Id = id,
            Name = "Test Game",
            Platform = "Test Platform",
            Price = 50.0
        };

        var hashCode1 = videoGame1.GetHashCode();
        var hashCode2 = videoGame2.GetHashCode();

        Assert.That(hashCode2, Is.EqualTo(hashCode1));
    }
}
