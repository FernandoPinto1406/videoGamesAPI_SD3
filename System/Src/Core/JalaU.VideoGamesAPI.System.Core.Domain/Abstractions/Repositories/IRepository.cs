﻿namespace JalaU.VideoGamesAPI.System.Core.Domain;

public interface IRepository<T>
{
    IEnumerable<T> GetAllEntities();
    T? GetEntityByCriteria(Func<T, bool> criteria);
    T SaveEntity(T videoGame);
    T UpdateEntity(T videoGame);
    T DeleteEntity(T entity);
}
