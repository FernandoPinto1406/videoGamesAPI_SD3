using System.Net;
using JalaU.VideoGamesAPI.System.Core.Domain;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace JalaU.VideoGamesAPI.System.Api.Restful.Controllers;

[Authorize]
[ApiController]
[Route("/videogames-api/v1/videoGames")]
[ServiceFilter(typeof(EnforceJsonResponseFilter))]
public class VideoGameController(ILogger<VideoGameController> logger, IVideoGameService service)
    : ControllerBase
{
    private readonly IVideoGameService _service = service;
    private readonly ILogger<VideoGameController> _logger = logger;

    [HttpGet]
    public ActionResult GetAllVideoGames()
    {
        IEnumerable<VideoGame> videoGames = _service.GetAllVideoGames();
        GetAllVideoGamesResponseDTO getAllVideoGamesResponseDTO =
            new(videoGames.Count(), videoGames.ToList());

        return Ok(getAllVideoGamesResponseDTO);
    }

    [HttpGet("{videoGameId}")]
    public ActionResult GetVideoGameById(string videoGameId)
    {
        var videoGame = _service.GetVideoGameByCriteria("id", videoGameId);
        return Ok(videoGame);
    }

    [Authorize(Policy = "UserAdmin")]
    [HttpPost]
    public ActionResult CreateVideoGame([FromBody] VideoGameRequestDTO videoGameDto)
    {
        var videoGame = _service.SaveVideoGame(videoGameDto);

        return StatusCode((int)HttpStatusCode.Created, videoGame);
    }

    [Authorize(Policy = "UserAdmin")]
    [HttpPut("{videoGameId}")]
    public ActionResult UpdateVideoGameById(
        [FromBody] VideoGameRequestDTO videoGameDto,
        string videoGameId
    )
    {
        var videoGame = _service.UpdateVideoGameById(videoGameDto, videoGameId);
        return Ok(videoGame);
    }

    [Authorize(Policy = "UserAdmin")]
    [HttpDelete("{videoGameId}")]
    public ActionResult DeleteVideoGameById(string videoGameId)
    {
        var videoGame = _service.DeleteVideoGameById(videoGameId);
        return Ok(videoGame);
    }
}
