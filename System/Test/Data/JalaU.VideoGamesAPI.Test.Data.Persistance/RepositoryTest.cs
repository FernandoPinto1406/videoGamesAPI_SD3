using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using JalaU.VideoGamesAPI.System.Core.Domain;
using JalaU.VideoGamesAPI.System.Data.DbConnection;
using Microsoft.EntityFrameworkCore;
using Moq;
using NUnit.Framework;

namespace JalaU.VideoGamesAPI.Test.Data.Persistance;

[TestFixture]
public class RepositoryTest
{
    private DbContextOptions<AppDbContext> _options;

    [SetUp]
    public void Setup()
    {
        _options = new DbContextOptionsBuilder<AppDbContext>()
            .UseInMemoryDatabase(databaseName: "test_database")
            .Options;
    }

    [Test]
    public void TestGetAllVideoGames()
    {
        using var context = new AppDbContext(_options);
        context.VideoGames.Add(
            new VideoGame
            {
                Id = Guid.NewGuid(),
                Name = "Test Game",
                Platform = "XBOX",
                Price = 50.0
            }
        );
        context.SaveChanges();
        var repository = new VideoGameRepository(context);

        var result = repository.GetAllEntities();

        Assert.That(result.Count(), Is.EqualTo(1));
    }

    [Test]
    public void TestGetVideoGameById()
    {
        var expectedId = Guid.NewGuid();
        using (var context = new AppDbContext(_options))
        {
            context.VideoGames.Add(
                new VideoGame
                {
                    Id = expectedId,
                    Name = "Test Game 2",
                    Platform = "Test Platform 2",
                    Price = 60.0
                }
            );
            context.SaveChanges();
        }

        using (var context = new AppDbContext(_options))
        {
            var repository = new VideoGameRepository(context);

            var result = repository.GetEntityByCriteria(videoGame => videoGame.Id == expectedId);

            Assert.That(result, Is.Not.Null);
            Assert.That(result.Id, Is.EqualTo(expectedId));
        }
    }

    [Test]
    public void TestGetVideoGameByName()
    {
        var expectedName = "Test Game";
        using var context = new AppDbContext(_options);
        var repository = new VideoGameRepository(context);

        var result = repository.GetEntityByCriteria(videoGame => videoGame.Name == expectedName);

        Assert.That(result, Is.Not.Null);
        Assert.That(result.Name, Is.EqualTo(expectedName));
    }

    [Test]
    public void TestSaveVideoGame()
    {
        var newVideoGame = new VideoGame
        {
            Id = Guid.NewGuid(),
            Name = "New Game",
            Platform = "New Platform",
            Price = 70.0
        };
        using var context = new AppDbContext(_options);
        var repository = new VideoGameRepository(context);

        var result = repository.SaveEntity(newVideoGame);

        Assert.That(result, Is.EqualTo(newVideoGame));
    }

    [Test]
    public void TestDeleteVideoGameById()
    {
        var videoGameToDelete = new VideoGame
        {
            Id = Guid.NewGuid(),
            Name = "Game to Delete",
            Platform = "Platform",
            Price = 80.0
        };
        using (var context = new AppDbContext(_options))
        {
            context.VideoGames.Add(videoGameToDelete);
            context.SaveChanges();
        }

        using (var context = new AppDbContext(_options))
        {
            var repository = new VideoGameRepository(context);

            var existingVideoGame = repository.GetEntityByCriteria(
                videoGame => videoGame.Id == videoGameToDelete.Id
            );
            var result = repository.DeleteEntity(existingVideoGame!);

            Assert.That(result, Is.EqualTo(videoGameToDelete));
        }
    }
}
