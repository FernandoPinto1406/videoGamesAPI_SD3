﻿using System.Threading;
using JalaU.VideoGamesAPI.System.Core.Domain;
using Microsoft.EntityFrameworkCore;

namespace JalaU.VideoGamesAPI.System.Data.DbConnection;

public class UserRepository(AppDbContext appDbContext) : IUserRepository
{
    private readonly AppDbContext _appDbContext = appDbContext;

    public User? GetEntityByCriteria(Func<User, bool> criteria)
    {
        return this._appDbContext.Users.FirstOrDefault(criteria);
    }

    public User SaveUser(User user)
    {
        _appDbContext.Users.Add(user);
        _appDbContext.SaveChanges();
        return user;
    }
}
