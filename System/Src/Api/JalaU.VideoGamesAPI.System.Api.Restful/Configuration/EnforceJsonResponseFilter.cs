﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Net.Http.Headers;

namespace JalaU.VideoGamesAPI.System.Api.Restful;

[AttributeUsage(AttributeTargets.All)]
public class EnforceJsonResponseFilter : Attribute, IActionFilter
{
    public void OnActionExecuted(ActionExecutedContext context)
    {
        if (context.Result is ObjectResult objectResult)
        {
            // Establecer el tipo de contenido como JSON
            context.HttpContext.Response.ContentType = "application/json";
        }
    }

    public void OnActionExecuting(ActionExecutingContext context)
    {
        // No es necesario implementar esta parte para este filtro
    }
}
