﻿using JalaU.VideoGamesAPI.System.Core.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace JalaU.VideoGamesAPI.System.Data.DbConnection;

public class VideoGameConfiguration : IEntityTypeConfiguration<VideoGame>
{
    public void Configure(EntityTypeBuilder<VideoGame> builder)
    {
        builder.HasIndex(videoGame => videoGame.Name).IsUnique();
    }
}
