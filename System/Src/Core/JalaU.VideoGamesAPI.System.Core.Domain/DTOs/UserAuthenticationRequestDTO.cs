﻿namespace JalaU.VideoGamesAPI.System.Core.Domain;

public class UserAuthenticationRequestDTO
{
    public required string UserName { get; set; }

    public required string Password { get; set; }
}
