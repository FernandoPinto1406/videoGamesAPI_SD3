﻿using System.Net;
using JalaU.VideoGamesAPI.System.Core.Domain;

namespace JalaU.VideoGamesAPI.System.Core.Application;

public class VideoGameValidatorUtil
{
    readonly List<MessageLogDTO> messageLogDTOs = [];

    public List<MessageLogDTO> ValidateEntityToSave(
        VideoGame? existingVideoGame,
        BaseRequestDTO baseRequestDTO
    )
    {
        var videoGameRequestDTO = ValidateVideoGameDTO(baseRequestDTO);
        messageLogDTOs.AddRange(
            EntityValidatorUtil<VideoGameRequestDTO>.ValidateEntityFields(videoGameRequestDTO)
        );
        CheckForDuplicateName(existingVideoGame);
        ValidateSpecificFields(videoGameRequestDTO);

        return messageLogDTOs;
    }

    public List<MessageLogDTO> ValidateEntityToUpdate(
        VideoGame? existingVideoGame,
        BaseRequestDTO baseRequestDTO
    )
    {
        VideoGameRequestDTO videoGameRequestDTO = ValidateVideoGameDTO(baseRequestDTO);
        CheckForDuplicateName(existingVideoGame);

        ValidateSpecificFields(videoGameRequestDTO);

        return messageLogDTOs;
    }

    private void CheckForDuplicateName(VideoGame? existingVideoGame)
    {
        if (existingVideoGame != null)
        {
            messageLogDTOs.Add(
                new MessageLogDTO(
                    (int)HttpStatusCode.Conflict,
                    $"The VideoGame with the name {existingVideoGame.Name} already exists."
                )
            );
        }
    }

    private void ValidateSpecificFields(VideoGameRequestDTO videoGameDto)
    {
        if (videoGameDto.Platform != null)
        {
            messageLogDTOs.AddRange(ValidatePlatform(videoGameDto.Platform));
        }
        if (videoGameDto.Price != 0)
        {
            messageLogDTOs.AddRange(ValidatePrice(videoGameDto.Price));
        }
    }

    private static IEnumerable<MessageLogDTO> ValidatePlatform(string platform)
    {
        return ValidPlatformValidatorUtil.ValidateSupportedPlatform(platform) ?? [];
    }

    private static IEnumerable<MessageLogDTO> ValidatePrice(double price)
    {
        return PositiveNumberValidatorUtil.ValidatePositiveNumber("price", price) ?? [];
    }

    private VideoGameRequestDTO ValidateVideoGameDTO(BaseRequestDTO baseRequestDTO)
    {
        try
        {
            return (VideoGameRequestDTO)baseRequestDTO;
        }
        catch (Exception exception)
        {
            messageLogDTOs.Add(
                new MessageLogDTO((int)HttpStatusCode.UnprocessableEntity, exception.Message)
            );
            throw new WrongDataException("errors", this.messageLogDTOs);
        }
    }
}
