﻿using JalaU.VideoGamesAPI.System.Core.Application;
using JalaU.VideoGamesAPI.System.Core.Domain;
using Microsoft.Extensions.Configuration;
using Moq;

namespace JalaU.VideoGamesAPI.Test.Core.Application;

[TestFixture]
public class AuthServiceTest
{
    private Mock<IUserRepository> _mockRepository;
    private AuthService _authService;

    [SetUp]
    public void Setup()
    {
        var configurationMock = new Mock<IConfiguration>();
        _mockRepository = new Mock<IUserRepository>();
        _authService = new AuthService(_mockRepository.Object, configurationMock.Object);
    }

    [Test]
    public void TestRegisterUserWithDuplicateUserName()
    {
        var existingUserName = "existingUserName";
        var userRegistrationDTO = new UserRegistrationRequestDTO
        {
            UserName = existingUserName,
            Password = "password2456!",
            Role = "USER"
        };

        _mockRepository
            .Setup(repo => repo.GetEntityByCriteria(It.IsAny<Func<User, bool>>()))
            .Returns(
                new User()
                {
                    Id = Guid.NewGuid(),
                    UserName = existingUserName,
                    Password = "aosidido12",
                    Role = "ADMIN"
                }
            );

        Assert.Throws<WrongDataException>(() => _authService.RegisterUser(userRegistrationDTO));
    }

    [Test]
    public void TestRegisterUserWithWeakPassword()
    {
        var userRegistrationDTO = new UserRegistrationRequestDTO
        {
            UserName = "newUser",
            Password = "weakPwd",
            Role = "ADMIN"
        };

        _mockRepository
            .Setup(repo => repo.GetEntityByCriteria(It.IsAny<Func<User, bool>>()))
            .Returns((User)null!);

        Assert.Throws<WrongDataException>(() => _authService.RegisterUser(userRegistrationDTO));
    }

    [Test]
    public void TestRegisterUserWithInvalidRole()
    {
        var invalidRole = "InvalidRole";
        var userRegistrationDTO = new UserRegistrationRequestDTO
        {
            UserName = "newUser",
            Password = "validPassword1",
            Role = invalidRole,
        };

        _mockRepository
            .Setup(repo => repo.GetEntityByCriteria(It.IsAny<Func<User, bool>>()))
            .Returns((User)null!);

        var exception = Assert.Throws<WrongDataException>(
            () => _authService.RegisterUser(userRegistrationDTO)
        );
        Assert.Multiple(() =>
        {
            Assert.That(exception.Message, Is.EqualTo("errors"));
            Assert.That(exception.MessageLogs, Has.Count.EqualTo(1));
        });
        Assert.That(
            exception.MessageLogs[0].Message,
            Is.EqualTo($"The field Role doesn't match the valid roles.")
        );
    }
}
