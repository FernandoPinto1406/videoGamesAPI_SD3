using System.Net;
using JalaU.VideoGamesAPI.System.Api.Restful.Controllers;
using JalaU.VideoGamesAPI.System.Core.Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;

namespace JalaU.VideoGamesAPI.Test.Api.Rest;

[TestFixture]
public class VideoGameControllerTest
{
    private Mock<IVideoGameService> _mockVideoGameService;
    private Mock<ILogger<VideoGameController>> _mockLogger;
    private VideoGameController _videoGameController;

    [SetUp]
    public void Setup()
    {
        _mockVideoGameService = new Mock<IVideoGameService>();
        _mockLogger = new Mock<ILogger<VideoGameController>>();
        _videoGameController = new VideoGameController(
            _mockLogger.Object,
            _mockVideoGameService.Object
        );
    }

    [Test]
    public void TestGetAllVideoGames()
    {
        _mockVideoGameService.Setup(service => service.GetAllVideoGames()).Returns([]);

        var result = _videoGameController.GetAllVideoGames();

        Assert.That(result, Is.InstanceOf(typeof(OkObjectResult)));
        var okResult = (OkObjectResult)result;
        Assert.That(okResult.Value, Is.Not.Null);
    }

    [Test]
    public void TestGetVideoGameByIdWithValidId()
    {
        var guidVideoGame = Guid.NewGuid();
        var videoGameId = guidVideoGame.ToString();
        var videoGameResponseDto = new VideoGame
        {
            Id = guidVideoGame,
            Name = "Test Game",
            Platform = "Test Platform",
            Price = 50.0
        };
        _mockVideoGameService
            .Setup(service => service.GetVideoGameByCriteria("id", videoGameId))
            .Returns(videoGameResponseDto);

        var result = _videoGameController.GetVideoGameById(videoGameId);

        Assert.That(result, Is.InstanceOf(typeof(OkObjectResult)));
        var okResult = (OkObjectResult)result;
        Assert.That(okResult.Value, Is.Not.Null);
    }

    [Test]
    public void TestCreateVideoGameWithValidData()
    {
        var videoGameDto = new VideoGameRequestDTO
        {
            Name = "Test Game",
            Platform = "Test Platform",
            Price = 50.0
        };

        var result = _videoGameController.CreateVideoGame(videoGameDto);

        Assert.That(result, Is.InstanceOf(typeof(ObjectResult)));
        var createdResult = (ObjectResult)result;
        Assert.That(createdResult.StatusCode, Is.EqualTo((int)HttpStatusCode.Created));
    }

    [Test]
    public void UpdateVideoGameByIdWithValidIdAndData()
    {
        var guidVideoGame = Guid.NewGuid();
        var videoGameId = guidVideoGame.ToString();
        var videoGameDto = new VideoGameRequestDTO
        {
            Name = "Updated Test Game",
            Platform = "Updated Test Platform",
            Price = 60.0
        };
        var videoGameResponseDto = new VideoGame
        {
            Id = guidVideoGame,
            Name = "Test Game",
            Platform = "Test Platform",
            Price = 50.0
        };
        _mockVideoGameService
            .Setup(service => service.UpdateVideoGameById(videoGameDto, videoGameId))
            .Returns(videoGameResponseDto);

        var result = _videoGameController.UpdateVideoGameById(videoGameDto, videoGameId);

        Assert.That(result, Is.InstanceOf(typeof(OkObjectResult)));
        var okResult = (OkObjectResult)result;
        Assert.That(okResult.Value, Is.Not.Null);
    }

    [Test]
    public void TestDeleteVideoGameByIdWithValidId()
    {
        var videoGameId = Guid.NewGuid();
        var videoGame = new VideoGame
        {
            Id = videoGameId,
            Name = "Test Game",
            Platform = "Test Platform",
            Price = 50.0
        };
        _mockVideoGameService
            .Setup(service => service.DeleteVideoGameById(videoGameId.ToString()))
            .Returns(videoGame);

        var result = _videoGameController.DeleteVideoGameById(videoGameId.ToString());

        Assert.That(result, Is.InstanceOf(typeof(OkObjectResult)));
        var okResult = (OkObjectResult)result;
        Assert.That(videoGame, Is.EqualTo(okResult.Value!));
    }
}
