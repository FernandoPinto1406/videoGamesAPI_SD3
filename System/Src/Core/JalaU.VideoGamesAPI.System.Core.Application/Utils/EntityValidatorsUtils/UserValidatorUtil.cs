﻿using System.Net;
using JalaU.VideoGamesAPI.System.Core.Domain;

namespace JalaU.VideoGamesAPI.System.Core.Application;

public class UserValidatorUtil
{
    readonly List<MessageLogDTO> messageLogDTOs = [];

    public List<MessageLogDTO> ValidateEntityToSave(
        User? existingVideoGame,
        UserRegistrationRequestDTO userRequestDTO
    )
    {
        messageLogDTOs.AddRange(
            EntityValidatorUtil<UserRegistrationRequestDTO>.ValidateEntityFields(userRequestDTO)
        );
        CheckForDuplicateUserName(existingVideoGame);
        ValidateSpecificFieldsForUserRegister(userRequestDTO);

        return messageLogDTOs;
    }

    private void CheckForDuplicateUserName(User? possibleAlreadyExistingUser)
    {
        if (possibleAlreadyExistingUser != null)
        {
            messageLogDTOs.Add(
                new MessageLogDTO(
                    (int)HttpStatusCode.Conflict,
                    $"The User with the username {possibleAlreadyExistingUser.UserName} already exists."
                )
            );
        }
    }

    private void ValidateSpecificFieldsForUserRegister(
        UserRegistrationRequestDTO userRegistrationRequestDTO
    )
    {
        if (userRegistrationRequestDTO.Role != null)
        {
            messageLogDTOs.AddRange(ValidateUserRole(userRegistrationRequestDTO.Role));
        }
        if (userRegistrationRequestDTO.Password != null)
        {
            messageLogDTOs.AddRange(ValidatePassword(userRegistrationRequestDTO.Password));
        }
    }

    private static IEnumerable<MessageLogDTO> ValidateUserRole(string possibleUserRole)
    {
        return ValidUserRoleValidatorUtil.ValidateSupportedUserRole(possibleUserRole) ?? [];
    }

    private static IEnumerable<MessageLogDTO> ValidatePassword(string possiblePassword)
    {
        return PasswordValidatorUtil.ValidateUserPassword(possiblePassword) ?? [];
    }
}
