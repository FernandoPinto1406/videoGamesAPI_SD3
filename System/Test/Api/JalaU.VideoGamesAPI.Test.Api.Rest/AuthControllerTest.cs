﻿using System.Net;
using JalaU.VideoGamesAPI.System.Api.Restful;
using JalaU.VideoGamesAPI.System.Api.Restful.Controllers;
using JalaU.VideoGamesAPI.System.Core.Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;

namespace JalaU.VideoGamesAPI.Test.Api.Rest;

[TestFixture]
public class AuthControllerTest
{
    private Mock<IAuthService> _mockAuthService;
    private Mock<ILogger<AuthController>> _mockLogger;
    private AuthController _authenticationController;

    [SetUp]
    public void Setup()
    {
        _mockAuthService = new Mock<IAuthService>();
        _mockLogger = new Mock<ILogger<AuthController>>();
        _authenticationController = new AuthController(_mockLogger.Object, _mockAuthService.Object);
    }

    [Test]
    public void TestRegisterWithDuplicateUserName()
    {
        var duplicateUserName = "existingUserName";
        var userRegistrationRequest = new UserRegistrationRequestDTO
        {
            UserName = duplicateUserName,
            Password = "password123",
            Role = "ADMIN"
        };
        List<MessageLogDTO> messageLogDTOs = [new(409, "Username already exists")];

        _mockAuthService
            .Setup(service => service.RegisterUser(It.IsAny<UserRegistrationRequestDTO>()))
            .Throws<WrongDataException>(() => new WrongDataException("error data", messageLogDTOs));

        WrongDataException? wrongDataException = null;
        try
        {
            _authenticationController.Register(userRegistrationRequest);
        }
        catch (WrongDataException exception)
        {
            wrongDataException = exception;
        }

        Assert.That(wrongDataException, Is.Not.Null);
        Assert.That(wrongDataException.MessageLogs, Is.Not.Null);
        Assert.That(wrongDataException.MessageLogs, Has.Count.EqualTo(1));
        Assert.Multiple(() =>
        {
            Assert.That(wrongDataException.MessageLogs[0].StatusCode, Is.EqualTo(409));
            Assert.That(
                wrongDataException.MessageLogs[0].Message,
                Is.EqualTo("Username already exists")
            );
        });
    }
}
