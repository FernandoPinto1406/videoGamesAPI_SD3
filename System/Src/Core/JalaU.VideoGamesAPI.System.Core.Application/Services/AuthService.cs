﻿using System.IdentityModel.Tokens.Jwt;
using System.Net;
using System.Security.Claims;
using System.Text;
using AutoMapper;
using JalaU.VideoGamesAPI.System.Core.Domain;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace JalaU.VideoGamesAPI.System.Core.Application;

public class AuthService : IAuthService
{
    private readonly IUserRepository _repository;
    private readonly Mapper _userMapper;
    private readonly UserValidatorUtil userValidatorUtil;
    private IConfiguration configuration;

    public AuthService(IUserRepository repository, IConfiguration configuration)
    {
        _repository = repository;
        this.userValidatorUtil = new();
        this.configuration = configuration;
        var _configUserMapper = new MapperConfiguration(configuration =>
        {
            configuration.CreateMap<UserRegistrationRequestDTO, User>().ReverseMap();
        });

        _userMapper = new Mapper(_configUserMapper);
    }

    public string RegisterUser(UserRegistrationRequestDTO userRegistrationDTO)
    {
        var duplicatedUserByUserName = _repository.GetEntityByCriteria(
            user => user.UserName == userRegistrationDTO.UserName
        );

        List<MessageLogDTO> messageLogDTOs = userValidatorUtil.ValidateEntityToSave(
            duplicatedUserByUserName,
            userRegistrationDTO
        );

        if (messageLogDTOs.Count > 0)
        {
            throw new WrongDataException("errors", messageLogDTOs);
        }

        var userToSave = _userMapper.Map<User>(userRegistrationDTO);
        var encryptedPassword = PasswordEncrypterUtil.EncryptPassword(userToSave.Password);
        userToSave.Password = encryptedPassword;

        var userCreated = _repository.SaveUser(userToSave);

        return GenerateToken(userCreated);
    }

    public string AuthenticateUser(UserAuthenticationRequestDTO userAuthDTO)
    {
        var existingUser = _repository.GetEntityByCriteria(
            user => user.UserName == userAuthDTO.UserName
        );

        if (existingUser == null)
        {
            var message = $"The user with the username '{userAuthDTO.UserName}' doesn't exist.";
            throw new EntityNotFoundException(message);
        }

        var encryptedPassword = PasswordEncrypterUtil.EncryptPassword(userAuthDTO.Password);

        if (!existingUser.Password.Equals(encryptedPassword))
        {
            var message = $"The password provided for user '{userAuthDTO.UserName}' is incorrect.";
            throw new InvalidCredentialsException(message);
        }

        return GenerateToken(existingUser);
    }

    private string GenerateToken(User user)
    {
        var claims = new[]
        {
            new Claim(ClaimTypes.Name, user.UserName),
            new Claim(ClaimTypes.Role, user.Role)
        };
        var key = new SymmetricSecurityKey(
            Encoding.UTF8.GetBytes(configuration.GetSection("JWT:Key").Value!)
        );

        var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);

        var securityToken = new JwtSecurityToken(
            claims: claims,
            expires: DateTime.Now.AddMinutes(60),
            signingCredentials: credentials
        );

        string token = new JwtSecurityTokenHandler().WriteToken(securityToken);

        return token;
    }
}
