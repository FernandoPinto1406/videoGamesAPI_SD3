﻿using System.Net;
using JalaU.VideoGamesAPI.System.Core.Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace JalaU.VideoGamesAPI.System.Api.Restful;

[ApiController]
[Route("/videogames-api/v1/users")]
[ServiceFilter(typeof(EnforceJsonResponseFilter))]
public class AuthController(ILogger<AuthController> logger, IAuthService service) : ControllerBase
{
    private readonly IAuthService _service = service;

    private readonly ILogger<AuthController> _logger = logger;

    [HttpPost]
    public ActionResult Register([FromBody] UserRegistrationRequestDTO userRegistrationRequest)
    {
        var userToken = _service.RegisterUser(userRegistrationRequest);
        AuthenticationResponseDTO authenticationResponseDTO = new(userToken);
        return StatusCode((int)HttpStatusCode.Created, authenticationResponseDTO);
    }

    [HttpPost("authentication")]
    public ActionResult Login([FromBody] UserAuthenticationRequestDTO userAuthenticationRequestDTO)
    {
        var userToken = _service.AuthenticateUser(userAuthenticationRequestDTO);
        AuthenticationResponseDTO authenticationResponseDTO = new(userToken);
        return StatusCode((int)HttpStatusCode.OK, authenticationResponseDTO);
    }
}
