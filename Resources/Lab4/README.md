# Simple Video Games API 
### Activity 1 — Lab 4 — SD3

By Fernando Pinto Villarroel

The objective of this activity was to learn how APIs are created using C#. This is the first time we have had the opportunity to use C# to develop an API from scratch. Since we have recently had the experience of developing an API In Java, I must say that developing an API in both languages is a completely different and challenging experience.

For that reason, after studying and following the recommendations made during classes, I decided to structure the project applying the **Three-Tier Architecture Style**, which allowed me to separate concerns and responsibilities in different libraries that then are used by the _Server_ to provide the API Service. That's why, the project is divided into the API Layer (Presentation Layer), Core Layer (Business Logic Layer, composed of the Application and Domain sub-layers), and Data Layer (Data Access Layer).

On the other hand, this is an API based on the **REST Architecture Style for APIS**. Being so, I tried to follow the main REST Principles (good naming of resources, good use of the HTTP Verbs, returning JSON objects as responses in the controller, etc). I must also mention that, within the Data Access Layer, I made use of the **Repository Pattern**, which also allowed me to separate the logic for accessing data from the rest of the application's business logic.

I also applied API First, since I first developed the API Definition in Swagger (You can find it in a YAML format file at the main directory) and it allowed me to better understand the proper status codes that must be returned, and so on.

Furthermore, during the development process, I had in mind a possible type of database change (SQL -> NoSQL), and I tried to make the System as modular as possible so it would be easier to go through this infrastructure change.

Finally, I'll explain how I achieved the development of the API through different Software Development Principles :

## PRINCIPLES APPLIED

**KISS (Keep It Simple, Stupid) Principle:**   I didn't forget to not make it unnecessarily complex. I implemented the necessary classes and interfaces to create a maintainable and easy-to-escalate system. 

**DRY (Don't Repeat Yourself):**  I focused on reducing the amount of repeated code by putting it in different classes with unique responsibilities. This way, we just need to call that class and we avoid repeating code (which would make the code hard to understand).


**SOC (Separation Of Concerns)** Thanks to applying the "Three-Tier Architecture Style", I could separate different concerns of the System at different Layers. That way, one layer doesn't know how the other layer implements its functionality since each layer is only responsible for its concern. It allows us to achieve modularity and scalability.

**Single Responsibility:** Each class has just one responsibility, so it allows me to achieve High Cohesion (Every class only has its corresponding methods) and Loose Coupling (Components are not all directly connected). I divided different responsibilities into different classes, so we don't end up having God Classes. Thanks to the separation of responsibilities in different classes, it would be easy to add a new resource and its proper Controller, Service, and Repository.

**Open/Closed Principle:** Thanks to the fact that we depend on Abstractions (Interfaces for defining the actions of the Service and Repository), we can add new concrete implementations and Inject them into the Abstraction, achieving an open-to-extension and closed-to-modification system.

**Dependency Inversion:** The system depends on abstractions, not concrete implementations. Classes that use the Abstractions don't depend on the concrete implementations to work, allowing us to inject at runtime the necessary classes with which it works.

Finally, I'll mention the most relevant technologies that I learned while developing an API on C# for the first time. 

## Some new things I've learned about C#
- **Entity Framework Core:** Entity Framework Core is an object-relational mapping (ORM) framework for .NET that allows us to work with databases using .NET objects, eliminating the need for much of the data-access code. 

- **AutoMapper:** AutoMapper simplifies the mapping between different .NET object types (e.g., from a DTO to an Entity, or vice versa), reducing repetitive code and increasing productivity.

- **Data Annotations:** Data Annotations provide a way to apply validation rules and constraints directly to entity classes, enhancing data integrity and consistency. 

- **Approaches for Unique Fields of an Entity:** There are three main approaches to setting a unique field in an entity class: Data Annotation, Fluent API, and Manual checking. As you can find in the code, I applied the first two, but since they were not working, I had to also apply the third approach. I didn't set this constraint at the database level, because I wanted to learn how to do it at the code level.

- **Dependency Injection:** Dependency Injection facilitates loose coupling and modular design by allowing components to be easily replaced or modified, making the system more scalable and adaptable to changes such as switching between SQL and NoSQL databases without requiring significant code modifications. In this case, this dependency Injection is set in the Program.cs class.

### DB CONFIGURATION
#### Initializing the docker-compose.yaml file:
![img.png](/Images/dockerComposeEvidence.png)  

#### Connecting to the databae using the **MySQL by Wejian Chen** plugin:
![img.png](/Images/dataBaseConnectionEvidence.png)  

#### Creation of the database using the init.sql file:
![img.png](/Images/databaseCreationEvidence.png)  

### UML DIAGRAM
```mermaid
---
title: Lab 4 - UML - Fernando Pinto - SD 3 - Section B
---
classDiagram
namespace PRESENTATION{
    class VideoGameController {
        -ILogger<VideoGameController> _logger
        -IVideoGameService _service
        +GetAllVideoGames() : ActionResult
        +GetVideoGameById(videoGameId: string) : ActionResult
        +CreateVideoGame(videoGameDto: VideoGameRequestDTO) : ActionResult
        +UpdateVideoGameById(videoGameDto: VideoGameRequestDTO, videoGameId: string) : ActionResult
        +DeleteVideoGameById(videoGameId: string) : ActionResult
    }
}
namespace BUSINESS_LOGIC{
    class VideoGameService {
        -IVideoGameRepository _repository
        -Mapper _videoGameMapper
        +GetAllVideoGames() : List<VideoGameResponseDTO>
        +GetVideoGameById(id: string) : VideoGameResponseDTO
        +SaveVideoGame(videoGameDto: VideoGameRequestDTO) : VideoGameResponseDTO
        +UpdateVideoGameById(videoGameDto: VideoGameRequestDTO, id: string) : VideoGameResponseDTO
        +DeleteVideoGameById(id: string) : VideoGameResponseDTO
    }
        class VideoGame {
        -Guid Id
        -string Name
        -string Platform
        -double Price
    }

    class IVideoGameService {
        +GetAllVideoGames() : List<VideoGameResponseDTO>
        +GetVideoGameById(id: string) : VideoGameResponseDTO
        +SaveVideoGame(videoGameDto: VideoGameRequestDTO) : VideoGameResponseDTO
        +UpdateVideoGameById(videoGameDto: VideoGameRequestDTO, id: string) : VideoGameResponseDTO
        +DeleteVideoGameById(id: string) : VideoGameResponseDTO
    }
    class IVideoGameRepository {
        +GetAllVideoGames() : IEnumerable<VideoGame>
        +GetVideoGameById(id: Guid) : VideoGame?
        +GetVideoGameByName(name: string) : VideoGame?
        +SaveVideoGame(videoGame: VideoGame) : VideoGame
        +DeleteVideoGameById(id: Guid) : VideoGame?
    }
}
namespace DATA_ACCESS{
    class VideoGameRepository {
        -AppDbContext _appDbContext
        +GetAllVideoGames() : IEnumerable<VideoGame>
        +GetVideoGameById(id: Guid) : VideoGame?
        +GetVideoGameByName(name: string) : VideoGame?
        +SaveVideoGame(videoGame: VideoGame) : VideoGame
        +DeleteVideoGameById(id: Guid) : VideoGame?
    }
}


    VideoGameController <-- IVideoGameService
    IVideoGameService <-- VideoGameService
    VideoGameService <-- IVideoGameRepository
    IVideoGameRepository <-- VideoGameRepository
    VideoGameRepository --> AppDbContext
    VideoGame --> IVideoGameRepository
  
```
![img.png](/Images/UmlLab4.png)  

# Execution
- **Note:** Although I'll be proving the API with the auto-generated swagger documentation of ASP.NET Core Framework, I'll adjoint the links to the definition and documentation in Swagger that also allows to prove the API in a more documented way:

    - **Swagger Definition:** https://app.swaggerhub.com/apis/FernandoPintoVillarroel/VideoGamesAPI/1.0.0 
    - **Swagger Documentation:** https://app.swaggerhub.com/apis-docs/FernandoPintoVillarroel/VideoGamesAPI/1.0.0#/videoGames/addVideoGame

### Execution Evidence: GET VideoGames
![img_1.png](/Images/ExecutionEvidence/GETExecutionEvidence.png)    

### Execution Evidence: POST VideoGame
![img_1.png](/Images/ExecutionEvidence/POSTExecutionEvidence.png)    

### Execution Evidence: GET VideoGame By Id
![img_1.png](/Images/ExecutionEvidence/GETbyIdExecutionEvidence.png)     

### Execution Evidence: PUT VideoGame By Id
![img_1.png](/Images/ExecutionEvidence/PUTExecutionEvidence.png)    

### Execution Evidence: DELETE VideoGame By Id
![img_1.png](/Images/ExecutionEvidence/DELETEExecutionEvidence.png)    


// Note: When a VideoGame is created without a 'price' field, by default its price will be 0 (free videoGame).

### Errors Handling Evidence 1
![img_1.png](/Images/ErrorHandlingEvidence/ErrorHandlingEvidence1.png)    

### Errors Handling Evidence 2
![img_1.png](/Images/ErrorHandlingEvidence/ErrorHandlingEvidence2.png)    

### Errors Handling Evidence 3
![img_1.png](/Images/ErrorHandlingEvidence/ErrorHandlingEvidence3.png)    


### Test cases running
![img_tests.png](/Images/TestsEvidences/testsRunningEvidenceLab4.png)    
