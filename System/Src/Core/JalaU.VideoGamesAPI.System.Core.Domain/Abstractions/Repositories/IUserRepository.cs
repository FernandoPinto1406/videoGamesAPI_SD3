﻿namespace JalaU.VideoGamesAPI.System.Core.Domain;

public interface IUserRepository
{
    User SaveUser(User user);

    User? GetEntityByCriteria(Func<User, bool> criteria);
}
