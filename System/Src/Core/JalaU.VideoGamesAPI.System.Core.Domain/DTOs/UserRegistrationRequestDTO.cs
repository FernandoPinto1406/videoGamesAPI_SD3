﻿namespace JalaU.VideoGamesAPI.System.Core.Domain;

public class UserRegistrationRequestDTO : BaseRequestDTO
{
    public required string UserName { get; set; }

    public required string Password { get; set; }

    public required string Role { get; set; }
}
