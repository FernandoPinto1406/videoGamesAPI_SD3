﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace JalaU.VideoGamesAPI.System.Core.Domain;

public class User
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    [Column("id")]
    public required Guid Id { get; set; }

    [Column("username")]
    [StringLength(200)]
    public required string UserName { get; set; }

    [Column("password")]
    [StringLength(200)]
    public required string Password { get; set; }

    [Column("role")]
    [StringLength(200)]
    public required string Role { get; set; }
}
