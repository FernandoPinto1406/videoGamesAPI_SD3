﻿namespace JalaU.VideoGamesAPI.System.Core.Domain;

public class MessageLogDTO(int statusCode, string message)
{
    public int StatusCode { get; set; } = statusCode;
    public string Message { get; set; } = message;
}
