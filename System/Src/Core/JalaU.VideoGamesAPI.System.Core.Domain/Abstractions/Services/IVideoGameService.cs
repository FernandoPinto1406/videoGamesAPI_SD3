﻿namespace JalaU.VideoGamesAPI.System.Core.Domain;

public interface IVideoGameService
{
    List<VideoGame> GetAllVideoGames();
    VideoGame GetVideoGameByCriteria(string field, string id);
    VideoGame SaveVideoGame(VideoGameRequestDTO entityRequestDTO);
    VideoGame UpdateVideoGameById(VideoGameRequestDTO entityRequestDTO, string id);
    VideoGame DeleteVideoGameById(string id);
}
