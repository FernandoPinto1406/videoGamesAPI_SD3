﻿using JalaU.VideoGamesAPI.System.Core.Domain;
using Microsoft.EntityFrameworkCore;

namespace JalaU.VideoGamesAPI.System.Data.DbConnection;

public class AppDbContext(DbContextOptions<AppDbContext> options) : DbContext(options)
{
    public DbSet<VideoGame> VideoGames { get; set; }
    public DbSet<User> Users { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        // I tried the Fluent API approach.
        modelBuilder.ApplyConfigurationsFromAssembly(GetType().Assembly);
    }
}
