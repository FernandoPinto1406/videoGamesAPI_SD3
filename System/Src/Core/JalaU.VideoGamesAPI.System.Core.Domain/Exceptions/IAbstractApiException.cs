﻿using System.Net;

namespace JalaU.VideoGamesAPI.System.Core.Domain;

public interface IAbstractApiException
{
    public HttpStatusCode StatusCode();
}
