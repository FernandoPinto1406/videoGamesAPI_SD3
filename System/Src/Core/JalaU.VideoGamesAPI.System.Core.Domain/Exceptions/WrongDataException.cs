﻿using System.Net;

namespace JalaU.VideoGamesAPI.System.Core.Domain;

public class WrongDataException(string message, List<MessageLogDTO> messageLogDTOs)
    : ApplicationException(message),
        IAbstractApiException
{
    public HttpStatusCode StatusCode() => HttpStatusCode.BadRequest;

    public List<MessageLogDTO> MessageLogs { get; set; } = messageLogDTOs;
}
