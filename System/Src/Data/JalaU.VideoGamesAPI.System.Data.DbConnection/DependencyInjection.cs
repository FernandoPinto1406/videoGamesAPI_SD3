﻿using JalaU.VideoGamesAPI.System.Core.Domain;
using Microsoft.Extensions.DependencyInjection;

namespace JalaU.VideoGamesAPI.System.Data.DbConnection;

public static class DependencyInjection
{
    public static IServiceCollection AddMySQLInfrastructure(this IServiceCollection services)
    {
        services.AddScoped<IRepository<VideoGame>, VideoGameRepository>();
        services.AddScoped<IUserRepository, UserRepository>();

        return services;
    }
}
