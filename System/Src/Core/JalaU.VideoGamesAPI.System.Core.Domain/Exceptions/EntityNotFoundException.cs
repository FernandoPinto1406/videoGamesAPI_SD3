﻿using System.Net;

namespace JalaU.VideoGamesAPI.System.Core.Domain;

public class EntityNotFoundException(string message)
    : ApplicationException(message),
        IAbstractApiException
{
    public HttpStatusCode StatusCode() => HttpStatusCode.NotFound;
}
