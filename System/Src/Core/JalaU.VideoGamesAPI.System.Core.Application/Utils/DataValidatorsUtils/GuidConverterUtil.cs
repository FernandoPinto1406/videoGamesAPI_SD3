﻿using System.Net;
using JalaU.VideoGamesAPI.System.Core.Domain;

namespace JalaU.VideoGamesAPI.System.Core.Application;

public class GuidConverterUtil
{
    public static Guid ValidateGuid(string input)
    {
        if (Guid.TryParse(input, out Guid result))
        {
            return result;
        }

        List<MessageLogDTO> messages = [];
        messages.Add(
            new MessageLogDTO(
                (int)HttpStatusCode.UnprocessableEntity,
                "The id given is not a correct GUID."
            )
        );

        throw new WrongDataException("errors", messages);
    }
}
